import {
  createMachine,
  assign,
  DoneInvokeEvent,
  spawn,
  Interpreter,
} from "xstate";
import { request, gql } from "graphql-request";
import { Generator, GeneratorResponse } from "../Data/Models";
import { TableMachine } from "./TableMachine";
import { Table, Item } from "../Data/Models";
import { ItemMachine } from "./ItemMachine";
import { GraphQLService } from "../Services/GraphQLService";

const getGeneratorByIdQuery = gql`
  query GetGeneratorById($id: Int!) {
    generators_by_pk(id: $id) {
      id
      name
      shared
      description
      user_id
      created_at
      updated_at
      tables {
        id
        name
        order
        generator_id
        created_at
        updated_at
        items {
          id
          text
          order
          table_id
          dice_range_start
          dice_range_end
          created_at
          updated_at
        }
      }
    }
  }
`;

const getGeneratorByIdRequest = (id: number) =>
  GraphQLService.request(getGeneratorByIdQuery, {
    id: id,
  });

const saveGeneratorMutation = gql`
  mutation SaveGeneratorMutation($generator: generators_insert_input!) {
    insert_generators_one(object: $generator) {
      id
      name
      shared
      description
      user_id
      created_at
      updated_at
      tables {
        id
      }
    }
  }
`;

const saveGeneratorMutationRequest = (generator: Generator) =>
  GraphQLService.request(
    saveGeneratorMutation,
    {
      generator: generator,
    }
  );

export interface GeneratorContext {
  id: number;
  generator: Generator;
  rolledItems: Item[];
}

export type GeneratorState =
  | {
      value: "starting";
      context: GeneratorContext;
    }
  | {
      value: "loadingGenerator";
      context: GeneratorContext;
    }
  | {
      value: "showGenerator";
      context: GeneratorContext;
    }
  | {
      value: "editGenerator";
      context: GeneratorContext;
    }
  | {
      value: "savingGenerator";
      context: GeneratorContext;
    }
  | {
      value: "failedToLoadGenerator";
      context: GeneratorContext;
    }
  | {
      value: "failedToSaveGenerator";
      context: GeneratorContext;
    };

export type GeneratorEvents =
  | SaveGeneratorEvent
  | UpdateGenerator
  | CreateTableEvent
  | RollEvent;

type SaveGeneratorEvent = {
  type: "SAVE_GENERATOR";
};

type UpdateGenerator = {
  type: "UPDATE_GENERATOR";
  payload: {
    field: "name" | "description" | "language";
    value: string;
  };
};

type CreateTableEvent = {
  type: "CREATE_TABLE";
};

type RollEvent = {
  type: "ROLL";
};

export const GeneratorMachine = createMachine<
  GeneratorContext,
  GeneratorEvents,
  GeneratorState
>(
  {
    id: "Generator",
    initial: "starting",
    states: {
      starting: {
        always: [
          { target: "loadingGenerator", cond: "checkId" },
          {
            target: "editGenerator",
            actions: assign((context) => ({
              ...context,
              generator: {
                ...context.generator,
                name: "",
                shared: true,
                description: "",
                language: "nl",
                user_id: 1,
              },
            })),
          },
        ],
      },
      loadingGenerator: {
        invoke: {
          src: (context, event) => getGeneratorByIdRequest(context.id),
          onDone: {
            target: "showGenerator",
            actions: assign({
              generator: (
                context,
                event: DoneInvokeEvent<{ generators_by_pk: GeneratorResponse }>
              ) => ({
                ...event.data.generators_by_pk,
                tables: event.data.generators_by_pk.tables.map((table) => {
                  const tableMachineWithContext = TableMachine.withContext({
                    table: {
                      ...((table as unknown) as Table),
                      items: table.items.map((item) => {
                        return spawn(ItemMachine.withContext({ item: item }), {
                          sync: true,
                          name: item.id.toString(),
                        });
                      }),
                    },
                  });
                  return spawn(tableMachineWithContext, {
                    sync: true,
                    name: table.id.toString(),
                  });
                }),
              }),
            }),
          },
          onError: {
            target: "failedToLoadGenerator",
          },
        },
      },
      editGenerator: {
        on: {
          UPDATE_GENERATOR: {
            actions: assign({
              generator: (context, event) =>
                Object.assign({}, context.generator, {
                  [event.payload.field]: event.payload.value,
                }),
            }),
          },
          SAVE_GENERATOR: {
            target: "savingGenerator",
          },
        },
      },
      showGenerator: {
        on: {
          CREATE_TABLE: {
            actions: assign((context) => ({
              ...context,
              generator: {
                ...context.generator,
                tables: context.generator.tables
                  ? [
                      ...context.generator.tables,
                      spawn(
                        TableMachine.withContext({
                          table: {
                            name: "",
                            order: context.generator.tables.length + 1,
                            generator_id: context.generator.id as number,
                            items: [],
                          },
                        })
                      ),
                    ]
                  : [
                      spawn(
                        TableMachine.withContext({
                          table: {
                            name: "",
                            order: 1,
                            generator_id: context.generator.id as number,
                            items: [],
                          },
                        })
                      ),
                    ],
              },
            })),
          },
          ROLL: {
            actions: assign((context) => ({
              ...context,
              rolledItems: roll(context) as Item[],
            })),
          },
        },
      },
      savingGenerator: {
        invoke: {
          src: (context, event) =>
            saveGeneratorMutationRequest(context.generator),
          onDone: {
            actions: assign({
              generator: (
                context,
                event: DoneInvokeEvent<{ insert_generators_one: Generator }>
              ) => event.data.insert_generators_one,
            }),
            target: "showGenerator",
          },
          onError: {
            target: "failedToSaveGenerator",
          },
        },
      },

      failedToLoadGenerator: {},
      failedToSaveGenerator: {},
    },
  },
  {
    guards: {
      checkId: (context) => !!context.id,
    },
  }
);

const roll = (context: GeneratorContext): (Item | undefined)[] => {
  return context.generator.tables?.map((table) => {
    const min = Math.min.apply(
      Math,
      (table.machine.context?.table.items?.map(
        (item) => item.machine.context?.item.dice_range_start
      ) as unknown) as number[]
    );
    const max = Math.max.apply(
      Math,
      (table.machine.context?.table.items?.map(
        (item) => item.machine.context?.item.dice_range_end
      ) as unknown) as number[]
    );
    const roll = Math.floor(Math.random() * max) + min;
    const selectedItem = table.machine.context?.table.items.find(
      (item) =>
        roll >= (item.machine.context?.item.dice_range_start as number) &&
        roll <= (item.machine.context?.item.dice_range_end as number)
    )?.machine.context?.item;
    return selectedItem;
  });
};
