import { createMachine, assign } from "xstate";
import { GraphQLService } from "../Services/GraphQLService";
import { deleteLocalstorage } from "../Services/LocalStorageService";

interface AuthContext {
  auth: {
    user: {
      id: number;
      email: string;
      allowed_roles: string;
      default_role: string;
    };
    success: boolean;
    token: string;
    expiresIn: number;
  };
  fields: {
    signInEmail: string;
    signInPassword: string;
    signUpEmail: string;
    signUpPassword: string;
  };
}

const fetchSignIn = (email: string, password: string) =>
  fetch("http://localhost:3001/get-token", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email, password }),
  }).then((res) => res.json());

const fetchSignUp = (email: string, password: string) =>
  fetch("http://localhost:3001/create-account", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email, password }),
  }).then((res) => res.json());

type AuthState =
  | {
      value: "logedOut";
      context: AuthContext;
    }
  | {
      value: "signingUp";
      context: AuthContext;
    }
  | {
      value: "signingIn";
      context: AuthContext;
    }
  | {
      value: "failed";
      context: AuthContext;
    }
  | {
      value: "logedIn";
      context: AuthContext;
    };

type AuthEvents = SignIn | Logout | UpdateField | SignUp | Restore;

type SignIn = {
  type: "SIGNIN";
};

type SignUp = {
  type: "SIGNUP";
};

type Logout = {
  type: "LOGOUT";
};

type Restore = {
  type: "RESTORE";
  payload: {
    user: {
      id: number;
      email: string;
      allowed_roles: string;
      default_role: string;
    };
    success: boolean;
    token: string;
    expiresIn: number;
  };
};

type UpdateField = {
  type: "UPDATE_FIELD";
  payload: {
    field: "signInEmail" | "signInPassword" | "signUpEmail" | "signUpPassword";
    value: string;
  };
};

export const authMachine = createMachine<AuthContext, AuthEvents, AuthState>({
  id: "auth",
  initial: "logedOut",
  states: {
    logedOut: {
      on: {
        RESTORE: {
          actions: assign({ auth: (context, event) => event.payload }),
          target: "logedIn",
        },
        SIGNUP: "signingUp",
        SIGNIN: "signingIn",
        UPDATE_FIELD: {
          actions: assign({
            fields: (context, event) =>
              Object.assign({}, context.fields, {
                [event.payload.field]: event.payload.value,
              }),
          }),
        },
      },
    },
    signingIn: {
      invoke: {
        src: (context) =>
          fetchSignIn(
            context.fields.signInEmail,
            context.fields.signInPassword
          ),
        onDone: {
          target: "logedIn",
          actions: [
            assign({
              auth: (_, event) => event.data,
            }),
          ],
        },
        onError: "failed",
      },
    },
    signingUp: {
      invoke: {
        src: (context) =>
          fetchSignUp(
            context.fields.signUpEmail,
            context.fields.signUpPassword
          ),
        onDone: {
          target: "logedIn",
          actions: assign({
            auth: (_, event) => event.data,
          }),
        },
        onError: "failed",
      },
    },
    failed: {
      // on: { LOGIN: "loading" },
    },
    logedIn: {
      on: {
        LOGOUT: {
          target: "logedOut",
          actions: () => deleteLocalstorage("rt-auth"),
        },
      },
    },
  },
});
