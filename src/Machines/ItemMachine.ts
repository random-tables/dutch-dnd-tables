import { createMachine, assign, DoneInvokeEvent } from "xstate";
import { request, gql } from "graphql-request";
import { Item, ItemResponse } from "../Data/Models";

const saveItemMutation = gql`
  mutation SaveItemMutation($item: items_insert_input!) {
    insert_items_one(object: $item) {
      id
      text
      order
      dice_range_start
      dice_range_end
      created_at
      updated_at
    }
  }
`;

const updateItemMutation = gql`
  mutation UpdateItemMutation(
    $id: items_pk_columns_input!
    $item: items_set_input
  ) {
    update_items_by_pk(pk_columns: $id, _set: $item) {
      id
      text
      order
      dice_range_start
      dice_range_end
      created_at
      updated_at
    }
  }
`;

const saveItemMutationRequest = (item: Item) =>
  request("http://localhost:8080/v1/graphql", saveItemMutation, {
    item: item,
  });

const updateItemMutationRequest = (item: Item) => {
  const id = item.id;
  const itemToPost = {
    order: item.order,
    table_id: item.table_id,
    dice_range_start: item.dice_range_start,
    dice_range_end: item.dice_range_end,
    text: item.text,
  };

  return request("http://localhost:8080/v1/graphql", updateItemMutation, {
    item: itemToPost,
    id: { id },
  });
};

export interface ItemContext {
  item: Item;
}

export type ItemState =
  | {
      value: "starting";
      context: ItemContext;
    }
  | {
      value: "showItem";
      context: ItemContext;
    }
  | {
      value: "editItem";
      context: ItemContext;
    }
  | {
      value: "savingItem";
      context: ItemContext;
    }
  | {
      value: "updatingItem";
      context: ItemContext;
    }
  | {
      value: "failedToSaveItem";
      context: ItemContext;
    };

export type ItemEvents = SaveItemEvent | UpdateItem | EditItemEvent;

type SaveItemEvent = {
  type: "SAVE_ITEM";
};

type EditItemEvent = {
  type: "EDIT_ITEM";
};

type UpdateItem = {
  type: "UPDATE_ITEM";
  payload: {
    field: "text" | "dice_range_start" | "dice_range_end";
    value: string | number;
  };
};

export const ItemMachine = createMachine<ItemContext, ItemEvents, ItemState>(
  {
    id: "Item",
    initial: "starting",
    states: {
      starting: {
        always: [
          { target: "showItem", cond: "checkId" },
          {
            target: "editItem",
          },
        ],
      },
      editItem: {
        on: {
          UPDATE_ITEM: {
            actions: assign({
              item: (context, event) =>
                Object.assign({}, context.item, {
                  [event.payload.field]: event.payload.value,
                }),
            }),
          },
          SAVE_ITEM: [
            {
              target: "savingItem",
              cond: (context) => !context.item.id,
            },
            { target: "updatingItem" },
          ],
        },
      },
      showItem: {
        on: {
          EDIT_ITEM: "editItem",
        },
      },
      savingItem: {
        invoke: {
          src: (context, event) => saveItemMutationRequest(context.item),
          onDone: {
            actions: assign({
              item: (
                context,
                event: DoneInvokeEvent<{ insert_items_one: Item }>
              ) => event.data.insert_items_one,
            }),
            target: "showItem",
          },
          onError: {
            target: "failedToSaveItem",
          },
        },
      },
      updatingItem: {
        invoke: {
          src: (context, event) => updateItemMutationRequest(context.item),

          onDone: {
            actions: assign({
              item: (
                context,
                event: DoneInvokeEvent<{ update_items_by_pk: ItemResponse }>
              ) => event.data.update_items_by_pk,
            }),
            target: "showItem",
          },
          onError: {
            target: "failedToSaveItem",
          },
        },
      },
      failedToSaveItem: {},
    },
  },
  {
    guards: {
      checkId: (context) => !!context.item.id,
    },
  }
);
