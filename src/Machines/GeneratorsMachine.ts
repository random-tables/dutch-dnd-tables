import { createMachine, assign } from "xstate";
import { request, gql } from "graphql-request";
import {Generator} from '../Data/Models';


const getAllGeneratorsQuery = gql`
  query GetAllGeneratorssQuery {
    generators {
      name
      description
      language
      id
    }
  }
`;

const getAllGeneratorsRequest = () => request(
  "http://localhost:8080/v1/graphql",
  getAllGeneratorsQuery
);

interface GeneratorsContext {
  generators: Generator[];
}

type GeneratorsState =
  | {
      value: "showGenerators";
      context: GeneratorsContext;
    }
  | {
      value: "loading";
      context: GeneratorsContext;
    }
  | {
      value: "failed";
      context: GeneratorsContext;
    };

type GeneratorsEvents = {
  type: "RETRY";
};

export const GeneratorsMachine = createMachine<
  GeneratorsContext,
  GeneratorsEvents,
  GeneratorsState
>({
  id: "Generators",
  initial: "loading",
  states: {
    showGenerators: {},
    loading: {
      invoke: {
        src: getAllGeneratorsRequest,
        onDone: {
          target: "showGenerators",
          actions: assign({
            generators: (_, event) => event.data.generators,
          }),
        },
        onError: "failed",
      },
    },
    failed: {
      on: { RETRY: "loading" },
    },
  },
});
