import { createMachine, assign, DoneInvokeEvent, spawn } from "xstate";
import { request, gql } from "graphql-request";
import { Table, TableResponse } from "../Data/Models";
import { ItemMachine } from "./ItemMachine";

const saveTableMutation = gql`
  mutation SaveTableMutation($table: tables_insert_input!) {
    insert_tables_one(object: $table) {
      id
      name
      order
      created_at
      updated_at
      items {
        id
        text
        order
        dice_range_start
        dice_range_end
        created_at
        updated_at
      }
    }
  }
`;

const saveTableMutationRequest = (table: Table) =>
  request("http://localhost:8080/v1/graphql", saveTableMutation, {
    table: table,
  });

const updateTableMutation = gql`
  mutation UpdateTableMutation(
    $id: tables_pk_columns_input!
    $table: tables_set_input
  ) {
    update_tables_by_pk(pk_columns: $id, _set: $table) {
      id
      name
      order
      created_at
      updated_at
      items {
        id
        text
        order
        dice_range_start
        dice_range_end
        created_at
        updated_at
      }
    }
  }
`;

const updateTableMutationRequest = (table: Table) => {
  const id = table.id;
  const tableToPost = {
    name: table.name,
    order: table.order,
    generator_id: table.generator_id,
  };

  return request("http://localhost:8080/v1/graphql", updateTableMutation, {
    table: tableToPost,
    id: { id },
  });
};

export interface TableContext {
  table: Table;
}

export type TableState =
  | {
      value: "starting";
      context: TableContext;
    }
  | {
      value: "showTable";
      context: TableContext;
    }
  | {
      value: "editTable";
      context: TableContext;
    }
  | {
      value: "updatingTable";
      context: TableContext;
    }
  | {
      value: "savingTable";
      context: TableContext;
    }
  | {
      value: "failedToSaveTable";
      context: TableContext;
    };

export type TableEvents =
  | SaveTableEvent
  | UpdateTable
  | CreateItemEvent
  | EditTableEvent;

type SaveTableEvent = {
  type: "SAVE_TABLE";
};

type UpdateTable = {
  type: "UPDATE_TABLE";
  payload: {
    field: "name";
    value: string;
  };
};

type CreateItemEvent = {
  type: "CREATE_ITEM";
};
type EditTableEvent = {
  type: "EDIT_TABLE";
};

export const TableMachine = createMachine<
  TableContext,
  TableEvents,
  TableState
>(
  {
    id: "Table",
    initial: "starting",
    states: {
      starting: {
        always: [
          { target: "showTable", cond: "checkId" },
          {
            target: "editTable",
          },
        ],
      },
      editTable: {
        on: {
          UPDATE_TABLE: {
            actions: assign({
              table: (context, event) =>
                Object.assign({}, context.table, {
                  [event.payload.field]: event.payload.value,
                }),
            }),
          },
          SAVE_TABLE: [
            {
              target: "savingTable",
              cond: (context) => !context.table.id,
            },
            { target: "updatingTable" },
          ],
        },
      },
      showTable: {
        on: {
          EDIT_TABLE: "editTable",
          CREATE_ITEM: {
            actions: assign((context) => ({
              ...context,
              table: {
                ...context.table,
                items: context.table.items
                  ? [
                      ...context.table.items,
                      spawn(
                        ItemMachine.withContext({
                          item: {
                            text: "",
                            dice_range_start: 0,
                            dice_range_end: 0,
                            order: context.table.items.length + 1,
                            table_id: (context.table.id as unknown) as number,
                          },
                        }),
                        { sync: true }
                      ),
                    ]
                  : [
                      spawn(
                        ItemMachine.withContext({
                          item: {
                            text: "",
                            order: 0,
                            dice_range_start: 0,
                            dice_range_end: 0,
                            table_id: (context.table.id as unknown) as number,
                          },
                        }),
                        { sync: true }
                      ),
                    ],
              },
            })),
          },
        },
      },
      savingTable: {
        invoke: {
          src: (context, event) => saveTableMutationRequest(context.table),
          onDone: {
            actions: assign({
              table: (
                context,
                event: DoneInvokeEvent<{ insert_tables_one: Table }>
              ) => event.data.insert_tables_one,
            }),
            target: "showTable",
          },
          onError: {
            target: "failedToSaveTable",
          },
        },
      },
      updatingTable: {
        invoke: {
          src: (context, event) => updateTableMutationRequest(context.table),
          onDone: {
            actions: assign({
              table: (
                context,
                event: DoneInvokeEvent<{ update_tables_by_pk: TableResponse }>
              ) => ({
                ...event.data.update_tables_by_pk,
                items: event.data.update_tables_by_pk.items.map((item) =>
                  spawn(ItemMachine.withContext({ item: item }), {
                    sync: true,
                    name: item.id.toString(),
                  })
                ),
              }),
            }),
            target: "showTable",
          },
          onError: {
            target: "failedToSaveTable",
          },
        },
      },
      failedToSaveTable: {},
    },
  },
  {
    guards: {
      checkId: (context) => !!context?.table?.id,
    },
  }
);
