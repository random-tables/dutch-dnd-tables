import React from "react";
import Layout from "./Components/Layout/Layout";
import GeneratorList from "./Components/Generators/GeneratorList";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import GeneratorDetails from "./Components/Generators/GeneratorDetails";
import Auth from "./Components/Auth/Auth";

function App() {
  
  return (
    <Router>
      <Layout>
        <Switch>
          <Route path="/generator/:id?">
            <GeneratorDetails />
          </Route>
          <Route path="/auth">
            <Auth />
          </Route>
          <Route path="/">
            <GeneratorList />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
