import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import useAuth from "../../StatefulHooks/useAuth";

const Auth = () => {
  const [state, send] = useAuth();
  const history = useHistory();

  useEffect(() => {
    if (state.matches("logedIn")) {
      history.push("/");
    }
  }, [state]);

  return (
    <div className="text-white flex">
      <div className="w-1/2">
        <h1 className="text-4xl mb-4">Login</h1>
        <input
          className="block border-2 border-deep-blue-700 rounded bg-deep-blue-900 text-white py-2 px-3 mb-3"
          value={state.context.fields?.signInEmail}
          placeholder="Email"
          type="text"
          onChange={(e) =>
            send({
              type: "UPDATE_FIELD",
              payload: { field: "signInEmail", value: e.target.value },
            })
          }
        />
        <input
          className="block border-2 border-deep-blue-700 rounded bg-deep-blue-900 text-white py-2 px-3 mb-3"
          value={state.context.fields?.signInPassword}
          placeholder="Password"
          type="password"
          onChange={(e) =>
            send({
              type: "UPDATE_FIELD",
              payload: { field: "signInPassword", value: e.target.value },
            })
          }
        />
        <button
          onClick={() => send({ type: "SIGNIN" })}
          className="border border-white text-white py-2 px-4 rounded"
        >
          Login
        </button>
      </div>
      <div className="w-1/2">
        <h1 className="text-4xl mb-4">Sign up</h1>
        <input
          className="block border-2 border-deep-blue-700 rounded bg-deep-blue-900 text-white py-2 px-3 mb-3"
          value={state.context.fields?.signUpEmail}
          placeholder="Email"
          type="text"
          onChange={(e) =>
            send({
              type: "UPDATE_FIELD",
              payload: { field: "signUpEmail", value: e.target.value },
            })
          }
        />
        <input
          className="block border-2 border-deep-blue-700 rounded bg-deep-blue-900 text-white py-2 px-3 mb-3"
          value={state.context.fields?.signUpPassword}
          placeholder="Password"
          type="password"
          onChange={(e) =>
            send({
              type: "UPDATE_FIELD",
              payload: { field: "signUpPassword", value: e.target.value },
            })
          }
        />
        <button
          onClick={() => send({ type: "SIGNUP" })}
          className="border border-white text-white py-2 px-4 rounded"
        >
          Sign up
        </button>
      </div>
    </div>
  );
};

export default Auth;
