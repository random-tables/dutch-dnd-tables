import React from "react";
import { Table as TableModel } from "../../../Data/Models";
import Table from "./Table";
import { Event, SCXML, EventData, State, Interpreter } from "xstate";
import {
  GeneratorEvents,
  GeneratorContext,
  GeneratorState,
} from "../../../Machines/GeneratorMachine";
import {
  TableContext,
  TableState,
  TableEvents,
} from "../../../Machines/TableMachine";
interface props {
  tables: Interpreter<TableContext, TableState, TableEvents>[];

  send: (
    event: GeneratorEvents | Event<GeneratorEvents>[] | SCXML.Event<any>,
    payload?: EventData | undefined
  ) => State<GeneratorContext, GeneratorEvents, any, GeneratorState>;
}

const Tables: React.FC<props> = ({ tables, send }) => {
  if (tables?.length === 0) {
    return (
      <>
        <p className="text-white">No tables present make one?</p>
      </>
    );
  } else {
    return (
      <>
        {tables?.map((table) => (
          <Table key={table.id} table={table}></Table>
        ))}
      </>
    );
  }
};

export default Tables;
