import { useActor, useMachine } from "@xstate/react";
import React from "react";
import { Interpreter, State } from "xstate";
import { Table as TableModel, Item as ItemModel } from "../../../Data/Models";
import {
  ItemContext,
  ItemEvents,
  ItemState,
} from "../../../Machines/ItemMachine";
import {
  TableContext,
  TableState,
  TableEvents,
} from "../../../Machines/TableMachine";
import Item from "../Items/Item";

interface props {
  table: Interpreter<TableContext, TableState, TableEvents>;
}

const Table: React.FC<props> = ({ table }) => {
  const [state, send] = useActor<
    TableEvents,
    State<TableContext, TableEvents, TableState>
  >(table);

  if (state.matches("failedToSaveTable")) {
    return <p className="text-white">failed</p>;
  }
  if (state.matches("savingTable")) {
    return <p className="text-white">failed</p>;
  }
  if (state.matches("showTable")) {
    return (
      <div className="bg-deep-blue-800 border-deep-blue-700 border rounded mb-4">
        <div className="flex p-4  justify-between mb-4 border-b border-deep-blue-700 item-center">
          <h1 className="text-white text-xl font-bold ">
            {state.context.table.name}
          </h1>
          <button
            className="border border-white text-white py-2 px-4 rounded"
            onClick={() => send({ type: "EDIT_TABLE" })}
          >
            Edit table
          </button>
        </div>

        <div className="flex p-4 font-bold text-white -mx-2">
          <div className="w-4/12 px-2">Dice roll</div>
          <div className="w-6/12 px-2">Text</div>
          <div className="w-2/12 px-2">Actions</div>
        </div>

        {state.context.table.items
          .sort(
            (a, b) =>
              (a.machine.context?.item.order as number) -
              (b.machine.context?.item.order as number)
          )
          .map((item) => (
            <Item key={item.id} item={item} />
          ))}

        <div className="p-4">
          <button
            className="border border-white text-white py-2 px-4 rounded"
            onClick={() => send({ type: "CREATE_ITEM" })}
          >
            Add item
          </button>
        </div>
      </div>
    );
  }
  if (state.matches("editTable")) {
    return (
      <div className="bg-deep-blue-800 border-deep-blue-700 border rounded mb-4">
        <div className="flex px-4 py-2 text-white -mx-2 items-center justify-between">
          <input
            className=" border-2 border-deep-blue-700 rounded bg-deep-blue-900 text-white py-2 px-3"
            value={state.context.table?.name}
            placeholder="Table name"
            type="text"
            onChange={(e) =>
              send({
                type: "UPDATE_TABLE",
                payload: { field: "name", value: e.target.value },
              })
            }
          />
          <button
            className="border border-bright-pink text-white py-2 px-4 rounded"
            onClick={() => send({ type: "SAVE_TABLE" })}
          >
            Save
          </button>
        </div>
      </div>
    );
  }

  return null;
};

export default Table;
