import React from "react";
import { useMachine } from "@xstate/react";
import { GeneratorsMachine } from "../../Machines/GeneratorsMachine";
import { Link } from "react-router-dom";

const Generators = () => {
  const [state, send] = useMachine(GeneratorsMachine, { devTools: process.env.NODE_ENV === "production" ? false : true, });

  if (state.matches("loading")) {
    return <p className="text-white">loading</p>;
  }
  if (state.matches("failed")) {
    return <p className="text-white">failed</p>;
  }
  if (state.matches("showGenerators")) {
    return (
      <>
        <h1 className="text-white text-xl font-bold mb-4">All generators</h1>
        {state.context.generators.map((gen) => (
          <Link key={gen.id} to={`/generator/${gen.id}`}>
            <div className="bg-deep-blue-800 p-4 rounded text-white mb-4 border border-deep-blue-700">
              <div className="flex justify-between items-center">
                <div>
                  <h2 className="font-head text-2xl">{gen.name}</h2>
                  <p>{gen.description}</p>
                </div>
                <div>
                  <img
                    className="w-8 h-8"
                    alt={gen.language}
                    src={`https://unpkg.com/language-icons/icons/${gen.language}.svg`}
                  />
                </div>
              </div>
            </div>
          </Link>
        ))}
      </>
    );
  } else {
    return null;
  }
};

export default Generators;
