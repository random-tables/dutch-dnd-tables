import { useActor, useMachine } from "@xstate/react";
import React from "react";
import { Interpreter, Actor, State } from "xstate";
import { Item as ItemModel } from "../../../Data/Models";
import {
  ItemContext,
  ItemEvents,
  ItemState,
} from "../../../Machines/ItemMachine";
interface Props {
  item: Interpreter<ItemContext, ItemState, ItemEvents>;
}

const Item: React.FC<Props> = ({ item }) => {
  const [state, send] = useActor<
    ItemEvents,
    State<ItemContext, ItemEvents, ItemState>
  >(item);

  if (state.matches("failedToSaveItem")) {
    return <p className="text-white">failed</p>;
  }
  if (state.matches("savingItem")) {
    return <p className="text-white">failed</p>;
  }
  if (state.matches("showItem")) {
    return (
      <>
        <div className="flex px-4 py-2 text-white -mx-2">
          <div className="w-4/12 px-2">
            {state.context.item.dice_range_start} -{" "}
            {state.context.item.dice_range_end}
          </div>
          <div className="w-6/12 px-2">{state.context.item.text}</div>
          <div className="w-2/12 px-2">
            <button onClick={() => send({ type: "EDIT_ITEM" })}>edit</button>
          </div>
        </div>
      </>
    );
  }
  if (state.matches("editItem")) {
    return (
      <>
        <div className="flex px-4 py-2 text-white -mx-2">
          <div className="w-4/12 flex  px-2">
            <input
              className="mr-4 w-24 border-2 border-deep-blue-700 rounded bg-deep-blue-900 text-white py-2 px-3"
              value={state.context.item?.dice_range_start}
              type="number"
              onChange={(e) =>
                send({
                  type: "UPDATE_ITEM",
                  payload: {
                    field: "dice_range_start",
                    value: e.target.valueAsNumber,
                  },
                })
              }
            />
            <input
              className="w-24 border-2 border-deep-blue-700 rounded bg-deep-blue-900 text-white py-2 px-3"
              value={state.context.item?.dice_range_end}
              type="number"
              onChange={(e) =>
                send({
                  type: "UPDATE_ITEM",
                  payload: {
                    field: "dice_range_end",
                    value: e.target.valueAsNumber,
                  },
                })
              }
            />
          </div>
          <div className="w-6/12  px-2">
            <input
              className="w-full border-2 border-deep-blue-700 rounded bg-deep-blue-900 text-white py-2 px-3"
              value={state.context.item?.text}
              placeholder="Item text"
              type="text"
              onChange={(e) =>
                send({
                  type: "UPDATE_ITEM",
                  payload: { field: "text", value: e.target.value },
                })
              }
            />
          </div>
          <div className="w-2/12  px-2">
            <button
              className="border border-bright-pink text-white py-2 px-4 rounded"
              onClick={() => send({ type: "SAVE_ITEM" })}
            >
              Save
            </button>
          </div>
        </div>
      </>
    );
  }
  return null;
};

export default Item;
