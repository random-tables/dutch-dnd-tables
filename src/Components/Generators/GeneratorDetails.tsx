import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useMachine } from "@xstate/react";
import { GeneratorMachine } from "../../Machines/GeneratorMachine";
import ISO6391 from "iso-639-1";
import Tables from "./Tables/Tables";

const Generator = () => {
  let { id } = useParams<{ id: string }>();
  const [state, send] = useMachine(GeneratorMachine, {
    devTools: process.env.NODE_ENV === "production" ? false : true,
    context: { id: Number(id) },
  });

  useEffect(() => {
    if (state.context.generator?.id) {
      window.history.replaceState(
        "",
        "",
        `/generator/${state.context.generator?.id}`
      );
    }
  }, [state.context]);

  if (
    state.matches("failedToLoadGenerator") ||
    state.matches("failedToSaveGenerator")
  ) {
    return <p className="text-white">failed</p>;
  }
  if (state.matches("loadingGenerator") || state.matches("savingGenerator")) {
    return <p className="text-white">failed</p>;
  }
  if (state.matches("editGenerator")) {
    return (
      <>
        <input
          className="w-full md:w-1/2 block mb-4 border-2 border-deep-blue-700 rounded bg-deep-blue-900 text-white py-3 px-4"
          value={state.context.generator?.name}
          placeholder="Generator name"
          type="text"
          onChange={(e) =>
            send({
              type: "UPDATE_GENERATOR",
              payload: { field: "name", value: e.target.value },
            })
          }
        />
        <textarea
          rows={16}
          className="w-full md:w-1/2 block mb-4 border-2 border-deep-blue-700 rounded bg-deep-blue-900 text-white py-3 px-4"
          placeholder="A description of the generator"
          value={state.context.generator?.description}
          onChange={(e) =>
            send({
              type: "UPDATE_GENERATOR",
              payload: { field: "description", value: e.target.value },
            })
          }
        />
        <select
          placeholder="Select language"
          value={state.context.generator?.language}
          onChange={(e) =>
            send({
              type: "UPDATE_GENERATOR",
              payload: { field: "language", value: e.target.value },
            })
          }
          className="w-full md:w-1/2 block mb-4 border-2 border-deep-blue-700 rounded bg-deep-blue-900 text-white py-3 px-4"
        >
          <option disabled={true} value="default">
            Select language
          </option>
          {ISO6391.getAllNames().map((name) => (
            <option key={name} value={ISO6391.getCode(name)}>
              {name}
            </option>
          ))}
        </select>
        <button
          className="border border-bright-pink text-white py-2 px-4 rounded"
          onClick={() => send({ type: "SAVE_GENERATOR" })}
        >
          Save
        </button>
      </>
    );
  }
  if (state.matches("showGenerator")) {
    return (
      <>
        {state.context.rolledItems && (
          <div className="flex justify-between mb-4 border border-white rounded text-white flex-col items-center">
            <h1>Rolled items:</h1>
            {state.context.rolledItems.map((item) => (
              <p key={item.id}>{item.text}</p>
            ))}
          </div>
        )}

        <div className="flex justify-between">
          <div>
            <h1 className="text-white text-4xl font-bold mb-1">
              {state.context.generator?.name}
            </h1>
            <p className="text-white mb-4">
              {state.context.generator?.description}
            </p>
          </div>
          <div>
            <button
              onClick={() => send("ROLL")}
              className="border bg-bright-pink text-white py-2 px-4 rounded"
            >
              Roll items!
            </button>
          </div>
        </div>

        <Tables send={send} tables={state.context.generator.tables} />
        <button
          onClick={() => send({ type: "CREATE_TABLE" })}
          className="border border-white text-white py-2 px-4 rounded"
        >
          New table
        </button>
      </>
    );
  }
  return null;
};

export default Generator;
