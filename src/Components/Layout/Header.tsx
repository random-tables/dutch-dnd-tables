import React from "react";
import { Link } from "react-router-dom";
import { send } from "xstate/lib/actionTypes";
import useAuth from "../../StatefulHooks/useAuth";

const Header = () => {
  const [state, send] = useAuth();
  return (
    <>
      <div className="container mx-auto py-4 flex justify-between items-center">
        <h1 className="font-head text-xl text-white">DND - tables</h1>
        <nav>
          <Link to="/">
            <button className="border border-white text-white py-2 px-4 rounded mr-4">
              all generators
            </button>
          </Link>
          {state.matches("logedIn") && (
            <Link to="/generator">
              <button className="border border-white text-white py-2 px-4 rounded mr-4">
                new generator
              </button>
            </Link>
          )}
          {state.matches("logedIn") ? (
            <button
              onClick={() => send({ type: "LOGOUT" })}
              className="border border-white text-white py-2 px-4 rounded"
            >
              log out
            </button>
          ) : (
            <Link to="/auth">
              <button className="border border-white text-white py-2 px-4 rounded">
                log in
              </button>
            </Link>
          )}
        </nav>
      </div>
      <div className="container mx-auto">
        <hr style={{ borderColor: "#3f454d" }} className="" />
      </div>
    </>
  );
};

export default Header;
