import React from "react";
import Header from "./Header";
import Footer from "./Footer";

const Layout: React.FC = ({ children }) => {
  return (
    <div className="bg-deep-blue-900 min-h-screen flex flex-col px-4">
      <div className="flex-shrink">
        <Header />
      </div>

      <main className="flex-grow container mx-auto py-4">{children}</main>
      <div className="flex-shrink">
        <Footer />
      </div>
    </div>
  );
};

export default Layout;
