import { Interpreter, StateMachine } from "xstate";
import { ItemContext, ItemEvents, ItemState } from "../Machines/ItemMachine";
import { TableContext, TableEvents, TableState } from "../Machines/TableMachine";

export type Generator = {
  id?: number;
  name: string;
  shared: boolean;
  description?: string;
  user_id: number;
  language: string; 
  created_at?: string;
  updated_at?: string;
  tables: Interpreter<TableContext, TableState, TableEvents>[];
};

export type Table = {
  id?: number;
  name: string;
  order: number;
  generator_id: number;
  created_at?: string;
  updated_at?: string;
  items: Interpreter<ItemContext, ItemState, ItemEvents>[];
};

export type Item = {
  id?: number;
  order: number;
  table_id: number;
  dice_range_start: number;
  dice_range_end: number;
  created_at?: string;
  updated_at?: string;
  text: string;
};


export type GeneratorResponse = {
  id: number;
  name: string;
  shared: boolean;
  description: string;
  user_id: number;
  language: string; 
  created_at: string;
  updated_at: string;
  tables: TableResponse[];
};


export type TableResponse = {
  id: number;
  name: string;
  order: number;
  generator_id: number;
  created_at: string;
  updated_at: string;
  items: ItemResponse[];
};

export type ItemResponse = {
  id: number;
  order: number;
  table_id: number;
  dice_range_start: number;
  dice_range_end: number;
  created_at: string;
  updated_at: string;
  text: string;
};