import { createStore } from "reusable";
import { useMachine } from "@xstate/react";
import { authMachine } from "../Machines/AuthMachine";
import { useEffect } from "react";
import {
  loadLocalstorage,
  saveLocalstorage,
} from "../Services/LocalStorageService";
import { GraphQLService } from "../Services/GraphQLService";

const useAuth = createStore(() => {
  const machine = useMachine(authMachine, {
    devTools: process.env.NODE_ENV === "production" ? false : true,
  });
  const [state, send] = machine;

  useEffect(() => {
    const prevAuth = loadLocalstorage("rt-auth");

    if (prevAuth && prevAuth.expiresIn > Date.now()) {
      send({ type: "RESTORE", payload: prevAuth });
    }
  }, []);

  useEffect(() => {
    if (state.matches("logedIn")) {
      saveLocalstorage("rt-auth", state.context.auth);
      GraphQLService.setHeader(
        "authorization",
        `Bearer ${state.context.auth.token}`
      );
    }
  }, [state]);
  return machine;
});

export default useAuth;
