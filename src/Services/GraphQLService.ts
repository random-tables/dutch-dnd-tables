import { GraphQLClient } from "graphql-request";

const endpoint = process.env.REACT_APP_GRAPHQL_ENDPOINT ? process.env.REACT_APP_GRAPHQL_ENDPOINT : 'http://localhost:8080/v1/graphql'

export const GraphQLService = new GraphQLClient(endpoint, {
    headers: {
        authorization: '',
    }
})
