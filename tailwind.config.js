module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      colors: {
        "deep-blue": {
          900: "#0f1621",
          800: "#272d37",
          700: "#3f454d",
          600: "#575c64",
          500: "#6f737a",
          400: "#878b90",
          300: "#9fa2a6",
          200: "#b7b9bc",
        },
        "bright-pink": "#f72060",
      },
    },
  },
  variants: {},
  plugins: [],
};
