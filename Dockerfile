FROM node:14.14 as build-stage

RUN mkdir /app

WORKDIR /app

ADD ./ /app

RUN yarn install

RUN NODE_ENV=production yarn build

FROM nginx:1.15

COPY --from=build-stage /app/build/ /usr/share/nginx/html

COPY --from=build-stage /app/deploy/default.conf /etc/nginx/conf.d/default.conf